import { Component } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {RegisterUser} from './register-user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent {
  hide = true;
  user: RegisterUser = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phone: ''
  };

  constructor(
    public authService: AuthService,
    public router: Router,
  ) {
  }

  register(): void {
    console.log(this.user);
    this.authService.register(this.user).subscribe(() => {
      const redirectUrl = '/login';
      // Redirect the user
      this.router.navigate([redirectUrl]);
    });
  }
}
