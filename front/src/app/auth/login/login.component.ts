import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent {
  message?: string;
  hide = true;
  username = '';
  password = '';

  constructor(
    public authService: AuthService,
    public router: Router,
    private snackBar: MatSnackBar,
    private app: AppComponent
  ) {
    this.setMessage();
  }

  setMessage(): void {
    this.message = 'You are logged ' + (this.authService.isLoggedIn ? 'in' : 'out');
    this.snackBar.open('You are logged ' + (this.authService.isLoggedIn ? 'in' : 'out'), 'Undo',
      {
      duration: 2000,
    });

  }

  login(): void {
    this.message = 'Trying to log in ...';

    this.authService.login(this.username, this.password).subscribe(() => {
      this.setMessage();
      if (this.authService.isLoggedIn) {
        this.app.userIsLogged = true;
        // Usually you would use the redirect URL from the auth service.
        // However to keep the example simple, we will always redirect to `/admin`.
        const redirectUrl = '/pets';

        // Redirect the user
        this.router.navigate([redirectUrl]);
      }
    });
  }

  logout(): void {
    this.authService.logout();
    this.app.userIsLogged = false;
    this.setMessage();
  }
}
