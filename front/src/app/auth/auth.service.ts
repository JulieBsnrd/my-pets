import { Injectable } from '@angular/core';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RegisterUser} from './register/register-user';


const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': '*',
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root',
})


export class AuthService {
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl?: string;

  constructor(private http: HttpClient) { }

  getBaseUrl(): string {
    return 'https://127.0.0.1:8000';
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post<any>(this.getBaseUrl() + '/api/login_check', {username: username, password: password}, httpOptions)
      .pipe(map(token => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', 'Bearer ' + token.token);
          this.isLoggedIn = true;
          return token;
        })
      );
  }

  register(user: RegisterUser): Observable<boolean> {
    return this.http.post<any>(this.getBaseUrl() + '/register',
      {
        firstName: user.firstName,
        lastName: user.lastName,
        username: user.email,
        email: user.email,
        password: user.password,
        phone: user.phone,
      }, httpOptions)
      .pipe(map(response => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          // localStorage.setItem('token', 'Bearer ' + token.token);
          // this.isLoggedIn = true;
          return response;
        })
      );
  }

  logout(): void {
    localStorage.removeItem('token');
    this.isLoggedIn = false;
  }

  private handleError(): any {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong.
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // Return an observable with a user-facing error message.
  //   return throwError(
  //     'Something bad happened; please try again later.');
   }
}
