import {Component, OnInit} from '@angular/core';
import {Router, RouterOutlet} from '@angular/router';
import { slideInAnimation } from './animations';
import {AuthService} from "./auth/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [ slideInAnimation ]
})
export class AppComponent implements OnInit {
  userIsLogged = false;
  title = 'My Pets';
  token = localStorage.getItem('token');

  ngOnInit() {
    if (null !== this.token) {
      this.userIsLogged = true;
    }
  }


  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
