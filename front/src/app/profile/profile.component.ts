import {Component, OnInit} from '@angular/core';
import {User} from "./user";
import {HttpClient} from "@angular/common/http";
import { USER } from './mock-user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  user: User = USER;

  constructor(private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    // return this.http.get('/me');
  }

}
