import { User } from './user';

export const USER: User =
  {
    id: 12,
    firstName: 'Blanche',
    lastName: 'Neige',
    email: 'blanche@dwarf.com',
    phone: '0612121212'
  };
