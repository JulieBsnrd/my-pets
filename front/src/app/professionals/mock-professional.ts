import { Professional } from './professional';

export const PROFESSIONAL: Professional[] = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Dr Bombasto' },
  { id: 14, name: 'Dr Celeritas' },
  { id: 18, name: 'Dr IQ' }
];
