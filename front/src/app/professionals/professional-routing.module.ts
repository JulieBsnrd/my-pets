import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProfessionalListComponent} from "./professional-list/professional-list.component";
import {AuthGuard} from "../auth/auth.guard";

const routes: Routes = [
  { path: 'professionals', component: ProfessionalListComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessionalRoutingModule { }
