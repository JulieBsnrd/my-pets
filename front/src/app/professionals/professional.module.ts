import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ProfessionalListComponent } from './professional-list/professional-list.component';
import { ProfessionalRoutingModule } from './professional-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProfessionalRoutingModule
  ],
  declarations: [
    ProfessionalListComponent
  ]
})
export class ProfessionalModule {}
