import { Component, OnInit } from '@angular/core';
import { Professional } from "../professional";
import { ProfessionalService } from '../professional.service';
import { MessageService } from '../../message.service';

@Component({
  selector: 'app-professional-list',
  templateUrl: './professional-list.component.html',
  styleUrls: ['./professional-list.component.sass']
})
export class ProfessionalListComponent implements OnInit {

  professionals?: Professional[];
  selectedProfessional?: Professional;

  constructor(private professionalService: ProfessionalService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getProfessionals()
  }

  getProfessionals(): void {
    this.professionalService.getProfessionals()
      .subscribe(professionals => this.professionals = professionals);
  }

  onSelect(professional: Professional): void {
    this.selectedProfessional = professional;
    this.messageService.add(`ProfessionalComponent: Selected professional id=${professional.id}`);
  }
}
