import { Injectable } from '@angular/core';
import { Professional } from './professional';
import { PROFESSIONAL } from './mock-professional';
import { Observable, of } from 'rxjs';
import { MessageService } from '../message.service';

@Injectable({
  providedIn: 'root'
})
export class ProfessionalService {

  constructor(private messageService: MessageService) { }

  getProfessionals(): Observable<Professional[]> {
    const professionals = of(PROFESSIONAL);
    this.messageService.add('ProfessionalService: fetched professionals');
    return professionals;
  }
}
