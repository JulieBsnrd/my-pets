import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { PetService } from '../pet.service';
import { Pet } from '../pet';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.sass']
})
export class PetDetailComponent implements OnInit {
  pet$: Observable<Pet>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: PetService
  ) {}

  ngOnInit(): void {
    const petId = this.route.snapshot.paramMap.get('id');
    console.log(petId);
    this.pet$ = this.service.getPet(petId);
  }

  gotoPets(pet: Pet): void {
    const petId = pet ? pet.id : null;
    // Pass along the pet id if available
    // so that the PetList component can select that pet.
    // Include a junk 'foo' property for fun.
    this.router.navigate(['/pets', { id: petId, foo: 'foo' }]);
  }
}
