import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

import { Pet } from './pet';
import { MessageService } from '../message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const TYPE_DOG = 1;
const TYPE_CAT = 2;
const TYPE_HORSE = 3;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: localStorage.getItem('token')
  })};

const baseUrl = 'https://127.0.0.1:8000';

@Injectable({
  providedIn: 'root',
})

export class PetService {
  constructor(private messageService: MessageService, private http: HttpClient) { }

  getPets(): Observable<Pet[]> {
    return this.http
      .get<Pet[]>(baseUrl + '/api/v1/pets', httpOptions)
      .pipe(map(data => _.values(data)));
  }

  getPet(id: string): Observable<Pet> {
    return this.http
      .get<Pet>(baseUrl + '/api/v1/pets/' + id, httpOptions)
      .pipe((data => _.values(data)));
  }

  public getPetTypeString(type: number): string {
    if (TYPE_DOG === type) {
      return 'dog';
    } else if (TYPE_CAT === type) {
      return 'cat';
    } else if (TYPE_HORSE === type) {
      return 'horse';
    } else {
      return 'invalid';
    }
  }
}
