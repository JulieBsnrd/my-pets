import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetFollowUpFormComponent } from './pet-follow-up-form.component';

describe('PetFollowUpFormComponent', () => {
  let component: PetFollowUpFormComponent;
  let fixture: ComponentFixture<PetFollowUpFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetFollowUpFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetFollowUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
