import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

import { PetService } from '../../pet.service';
import { Pet } from '../../pet';

@Component({
  selector: 'app-pet-follow-up-form',
  templateUrl: './pet-follow-up-form.component.html',
  styleUrls: ['./pet-follow-up-form.component.sass']
})
export class PetFollowUpFormComponent implements OnInit {
  pet$?: Observable<Pet>;
  step1 = true;
  step2 = false;
  step3 = false;
  notifyMeForVermifuge = false;
  notifyMeForVermifugeByEmail = false;
  notifyMeForVermifugeBySms = false;
  notifyMeForAntiParasitaire = false;
  notifyMeForAntiParasitaireByEmail = false;
  notifyMeForAntiParasitaireBySms = false;
  notifyMeForVaccin = false;
  notifyMeForVaccinByEmail = false;
  notifyMeForVaccinBySms = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: PetService
  ) {}

  ngOnInit() {
    // @ts-ignore
    this.pet$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getPet(params.get('id')))
    );
  }
}
