import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

import { PetService } from '../pet.service';
import { Pet } from '../pet';

@Component({
  selector: 'app-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.sass']
})
export class PetAddComponent implements OnInit {
  pet$?: Observable<Pet>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: PetService
  ) {}

  ngOnInit() {
    // @ts-ignore
    this.pet$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getPet(params.get('id')))
    );
  }
}
