import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetAddDetailComponent } from './pet-add-detail.component';

describe('PetAddDetailComponent', () => {
  let component: PetAddDetailComponent;
  let fixture: ComponentFixture<PetAddDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetAddDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetAddDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
