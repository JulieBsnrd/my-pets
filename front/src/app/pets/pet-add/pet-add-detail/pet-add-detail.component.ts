import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

import { PetService } from '../../pet.service';
import { Pet } from '../../pet';

@Component({
  selector: 'app-pet-add-detail',
  templateUrl: './pet-add-detail.component.html',
  styleUrls: ['./pet-add-detail.component.sass']
})
export class PetAddDetailComponent implements OnInit {
  pet$?: Observable<Pet>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: PetService
  ) {}

  ngOnInit(): void {
    // @ts-ignore
    this.pet$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getPet(params.get('id')))
    );
  }
}
