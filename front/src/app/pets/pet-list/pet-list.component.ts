import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { PetService } from '../pet.service';
import { Pet } from '../pet';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.sass']
})
export class PetListComponent implements OnInit {
  pets$: Observable<Pet[]>;

  constructor(
    private service: PetService,
  ) {}

  ngOnInit(): void {
    this.pets$ = this.service.getPets();
  }
}
