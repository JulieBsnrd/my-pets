import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PetListComponent } from './pet-list/pet-list.component';
import { PetDetailComponent } from './pet-detail/pet-detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { PetAddComponent } from './pet-add/pet-add.component';
import { PetFollowUpFormComponent } from './pet-add/pet-follow-up-form/pet-follow-up-form.component';
import { PetAddDetailComponent } from './pet-add/pet-add-detail/pet-add-detail.component';

const petsRoutes: Routes = [
  { path: 'pets',  component: PetListComponent, data: { animation: 'pets' }, canActivate: [AuthGuard] },
  { path: 'pets/add',  component: PetAddComponent, data: { animation: 'pets' } },
  { path: 'pets/add/details',  component: PetAddDetailComponent, data: { animation: 'pets' } },
  { path: 'pets/add/follow-up',  component: PetFollowUpFormComponent, data: { animation: 'pets' } },
  { path: 'pets/:id', component: PetDetailComponent, data: { animation: 'pet' }, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(petsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PetsRoutingModule { }
