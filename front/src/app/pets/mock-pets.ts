import { Pet } from './pet';

export const PETS: Pet[] = [
  { id: 12, name: 'Narco', type: 1},
  { id: 13, name: 'Bombasto', type: 3 },
  { id: 14, name: 'Celeritas', type: 1 },
  { id: 15, name: 'Magneta', type: 2 },
  { id: 16, name: 'RubberMan', type: 2 },
  { id: 17, name: 'Dynama', type: 1 },
  { id: 19, name: 'Magma', type: 1 },
  { id: 20, name: 'Tornado', type: 3 }
];
