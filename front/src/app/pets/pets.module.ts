import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PetListComponent } from './pet-list/pet-list.component';
import { PetDetailComponent } from './pet-detail/pet-detail.component';
import { PetAddComponent } from './pet-add/pet-add.component';
import {PetFollowUpFormComponent} from './pet-add/pet-follow-up-form/pet-follow-up-form.component';

import { PetsRoutingModule } from './pets-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {PetAddDetailComponent} from './pet-add/pet-add-detail/pet-add-detail.component';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatChipsModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatButtonModule,
    MatGridListModule,
    PetsRoutingModule
  ],
  declarations: [
    PetListComponent,
    PetDetailComponent,
    PetFollowUpFormComponent,
    PetAddDetailComponent,
    PetAddComponent
  ]
})
export class PetsModule {}
