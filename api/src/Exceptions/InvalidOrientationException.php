<?php
declare(strict_types=1);

namespace App\Exceptions;


/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class InvalidOrientationException extends \LogicException
{
    /**
     * @throws InvalidOrientationException
     * @param string $orientation
     */
    public static function throw(string $orientation): void
    {
        throw new self("Orientation \"$orientation\" doesnt exist.");
    }
}
