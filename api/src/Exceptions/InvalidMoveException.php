<?php
declare(strict_types=1);

namespace App\Exceptions;


/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class InvalidMoveException extends \LogicException
{
    /**
     * @throws InvalidMoveException
     * @param string $move
     */
    public static function throw(string $move): void
    {
        throw new self("Move \"$move\" doesnt exist.");
    }
}
