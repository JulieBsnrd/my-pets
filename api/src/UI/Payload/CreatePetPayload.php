<?php
declare(strict_types=1);

namespace App\UI\Payload;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreatePetPayload
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $type;

    public function __construct(Request $request)
    {
        $rootNode = $this->extractData($request);

        if (!isset($rootNode['name'])) {
            throw new BadRequestHttpException('Missing node "name"');
        }

        if (!isset($rootNode['type'])) {
            throw new BadRequestHttpException('Missing node "type"');
        }

        $this->name = $rootNode['name'];
        $this->type = $rootNode['type'];
    }

    public function resolve()
    {

    }

    private function extractData(Request $request): array
    {
        if (null === $request->getContent()) {
            throw new BadRequestHttpException('json payload is empty.');
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        if (null === $data) {
            throw new BadRequestHttpException(
                sprintf(
                    'Unable to decode a json "%s".',
                    $request->getContent()
                )
            );
        }

        return $data;
    }
}
