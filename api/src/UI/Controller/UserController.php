<?php
declare(strict_types=1);

namespace App\UI\Controller;

use App\Domain\Entity\User;
use App\Repository\UserRepository;
use App\UI\DTO\UserDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/api/v1/users", methods={"GET"}, name="get_users")
     * @return JsonResponse
     */
    public function getUsers(): JsonResponse
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findAll();

        // todo dto array of userdto
        // todo use normalizer
        $usersDTO = [];
        foreach ($users as $user) {
            $usersDTO[] = new UserDTO($user->getFirstName(), $user->getLastName());
        }

        return new JsonResponse($usersDTO, JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/v1/users/{id}", methods={"GET"}, name="get_user")
     * @param string $id
     * @return JsonResponse
     */
    public function getOneUser(string $id): JsonResponse
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->find($id);

        if (null === $user) {
            return new JsonResponse('User with id ' . $id . ' not found.', JsonResponse::HTTP_NOT_FOUND);
        }

        // todo use normalizer
        $userDTO = new UserDTO($user->getFirstName(), $user->getLastName());

        return new JsonResponse($userDTO, JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/v1/users", methods={"POST"}, name="create_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function createUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        $data = $this->extractData($request);
        // new CreateUserPayload($request);
        // todo put this in a Payload
        if (!isset($data['firstName'])) {
            throw new BadRequestHttpException('Missing node "firstName"');
        }

        if (!isset($data['lastName'])) {
            throw new BadRequestHttpException('Missing node "lastName"');
        }

        if (!isset($data['username'])) {
            throw new BadRequestHttpException('Missing node "username"');
        }

        if (!isset($data['password'])) {
            throw new BadRequestHttpException('Missing node "password"');
        }

        $user = new User(
            $data['username']
        );

        $user->setPassword($passwordEncoder->encodePassword($user, $data['password']));

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $userRepository->save($user);

        // todo use normalizer
        $userDTO = new UserDTO($user->getFirstName(), $user->getLastName());

        return new JsonResponse($userDTO, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("/api/v1/users/{id}", methods={"PUT"}, name="edit_user")
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function editUser(string $id, Request $request): JsonResponse
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->find($id);

        if (null === $user) {
            return new JsonResponse('User with id ' . $id . ' not found.', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = $this->extractData($request);
        if (!isset($data['firstName'])) {
            throw new BadRequestHttpException('Missing node "firstName"');
        }

        if (!isset($data['lastName'])) {
            throw new BadRequestHttpException('Missing node "lastName"');
        }

        if (!isset($data['username'])) {
            throw new BadRequestHttpException('Missing node "username"');
        }

        $user->setFirstName($data['firstName']);
        $user->setLastName($data['lastName']);
        $user->setUsername($data['Username']);

        $userRepository->save($user);

        // todo use normalizer
        $userDTO = new UserDTO($user->getFirstName(), $user->getLastName());

        return new JsonResponse($userDTO, JsonResponse::HTTP_OK);
    }

    private function extractData(Request $request): array
    {
        if (null === $request->getContent()) {
            throw new BadRequestHttpException('json payload is empty.');
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        if (null === $data) {
            throw new BadRequestHttpException(
                sprintf(
                    'Unable to decode a json "%s".',
                    $request->getContent()
                )
            );
        }

        return $data;
    }
}
