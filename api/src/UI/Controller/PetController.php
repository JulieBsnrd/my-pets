<?php
declare(strict_types=1);

namespace App\UI\Controller;

use App\Domain\Entity\Pet;
use App\Domain\Entity\User;
use App\Repository\PetRepository;
use App\UI\DTO\PetDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PetController extends AbstractController
{
    /**
     * @Route("/api/v1/pets", methods={"GET"}, name="get_my_pets")
     * @return JsonResponse
     */
    public function getMyPets(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var PetRepository $petRepository */
        $petRepository = $this->getDoctrine()->getRepository(Pet::class);
        $pets = $petRepository->findBy(['ownerId' => $user->getId()]);

        // todo dto array of petdto
        // todo use normalizer
        $petsDTO = [];
        foreach ($pets as $pet) {
            $petsDTO[] = new PetDTO($pet->getId(), $pet->getName(), $pet->getType());
        }

        return new JsonResponse($petsDTO, JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/v1/pets/{id}", methods={"GET"}, name="get_pet")
     * @param string $id
     * @return JsonResponse
     */
    public function getPet(string $id): JsonResponse
    {
        /** @var PetRepository $petRepository */
        $petRepository = $this->getDoctrine()->getRepository(Pet::class);
        $pet = $petRepository->find($id);

        if (null === $pet) {
            return new JsonResponse('Pet with id ' . $id . ' not found.', JsonResponse::HTTP_NOT_FOUND);
        }

        // todo use normalizer
        $petDTO = new PetDTO($pet->getId(), $pet->getName(), $pet->getType());

        return new JsonResponse($petDTO, JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/api/v1/pets", methods={"POST"}, name="create_pet")
     * @param Request $request
     * @return JsonResponse
     */
    public function createPet(Request $request): JsonResponse
    {
        $data = $this->extractData($request);
        // new CreatePetPayload($request);

        if (!isset($data['name'])) {
            throw new BadRequestHttpException('Missing node "name"');
        }

        if (!isset($data['type'])) {
            throw new BadRequestHttpException('Missing node "type"');
        }

        $pet = new Pet(
            $data['name'],
            $data['type']
        );

        /** @var PetRepository $petRepository */
        $petRepository = $this->getDoctrine()->getRepository(Pet::class);
        $petRepository->save($pet);

        // todo use normalizer
        $petDTO = new PetDTO($pet->getId(), $pet->getName(), $pet->getType());

        return new JsonResponse($petDTO, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("/api/v1/pets/{id}", methods={"PUT"}, name="edit_pet")
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function editPet(string $id, Request $request): JsonResponse
    {
        /** @var PetRepository $petRepository */
        $petRepository = $this->getDoctrine()->getRepository(Pet::class);
        $pet = $petRepository->find($id);

        if (null === $pet) {
            return new JsonResponse('Pet with id ' . $id . ' not found.', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = $this->extractData($request);
        if (!isset($data['name'])) {
            throw new BadRequestHttpException('Missing node "name"');
        }

        if (!isset($data['type'])) {
            throw new BadRequestHttpException('Missing node "type"');
        }

        $pet->setName($data['name']);
        $pet->setType($data['type']);

        $petRepository->save($pet);

        // todo use normalizer
        $petDTO = new PetDTO($pet->getId(), $pet->getName(), $pet->getType());

        return new JsonResponse($petDTO, JsonResponse::HTTP_OK);
    }

    private function extractData(Request $request): array
    {
        if (null === $request->getContent()) {
            throw new BadRequestHttpException('json payload is empty.');
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        if (null === $data) {
            throw new BadRequestHttpException(
                sprintf(
                    'Unable to decode a json "%s".',
                    $request->getContent()
                )
            );
        }

        return $data;
    }
}
