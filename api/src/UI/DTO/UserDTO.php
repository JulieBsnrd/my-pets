<?php
declare(strict_types=1);

namespace App\UI\DTO;

use App\UI\Normalizer\Json;

/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class UserDTO
//    implements \JsonSerializable
{
    /** @var string */
    public $firstName;

    /** @var string */
    public $lastName;

    public function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }
//    /**
//     * @inheritDoc
//     */
//    public function jsonSerialize()
//    {
//        return Json::objectNode([
//            'name' => $this->name,
//            'type' => $this->type
//        ]);
//    }

}
