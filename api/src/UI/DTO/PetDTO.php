<?php
declare(strict_types=1);

namespace App\UI\DTO;

use App\UI\Normalizer\Json;

/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class PetDTO
//    implements \JsonSerializable
{
    /** @var string */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $type;

    public function __construct(string $id, string $name, int $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
    }
//    /**
//     * @inheritDoc
//     */
//    public function jsonSerialize()
//    {
//        return Json::objectNode([
//            'name' => $this->name,
//            'type' => $this->type
//        ]);
//    }

}
