Lancer Symfony
symfony server:start

Lancer Docker
docker-compose up -d

Créer la base de données
php bin/console doctrine:database:create

Créer les tables
php bin/console doctrine:migration:migrate
